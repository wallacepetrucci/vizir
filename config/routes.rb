Rails.application.routes.draw do
	root 'fale_mais#result'
	resources :fale_mais
	resources :rates
	get 'result' => 'fale_mais#result'  # Rota para GET Result
	post 'result' => 'fale_mais#result' # Rota para POST formulario Calculadora

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
