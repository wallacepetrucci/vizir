Para criação da aplicação foi utilizado Ruby on Rails.
Banco de dados slite3, configurado por padrão nahora da instalação do Framework.
A aplicação foi feito em localhost com o servidor nativo que o rails disponibiliza.

Digite:  rails server
No terminal para subir o servidor, por padrão ele abre na localhost:3000

Minhas rotas estão apontando para a aplicação como root, mas precisa cadastrar novos planos e novas tarifas antes de iniciar.

- Clique no botão "Cadastrar Tarifa", preencha o cadastro e salve.
- Volte para a aplicação e clique no botão "Cadastrar Fale Mais", preencha os campos e salve.
- Agora com os pre-cadastros já feitos, pode utilizar a aplicação.
- Digite o DDD de origem, DDD de destino e o tempo gasto, escolha qual plano foi utilizado e clique em Calcular.

OBS: Foi utilizado a tabela 'exemplo', enviado no PDF do desafio para os pre-cadastros.