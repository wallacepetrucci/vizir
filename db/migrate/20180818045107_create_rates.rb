class CreateRates < ActiveRecord::Migration[5.2]
  def change
    create_table :rates do |t|
      t.integer :origin
      t.integer :destiny
      t.float :price_per_minute

      t.timestamps
    end
  end
end
