class CreateFaleMais < ActiveRecord::Migration[5.2]
  def change
    create_table :fale_mais do |t|
      t.string :name
      t.integer :minute

      t.timestamps
    end
  end
end
