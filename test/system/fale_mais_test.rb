require "application_system_test_case"

class FaleMaisTest < ApplicationSystemTestCase
  setup do
    @fale_mai = fale_mais(:one)
  end

  test "visiting the index" do
    visit fale_mais_url
    assert_selector "h1", text: "Fale Mais"
  end

  test "creating a Fale mai" do
    visit fale_mais_url
    click_on "New Fale Mai"

    fill_in "Minute", with: @fale_mai.minute
    fill_in "Name", with: @fale_mai.name
    click_on "Create Fale mai"

    assert_text "Fale mai was successfully created"
    click_on "Back"
  end

  test "updating a Fale mai" do
    visit fale_mais_url
    click_on "Edit", match: :first

    fill_in "Minute", with: @fale_mai.minute
    fill_in "Name", with: @fale_mai.name
    click_on "Update Fale mai"

    assert_text "Fale mai was successfully updated"
    click_on "Back"
  end

  test "destroying a Fale mai" do
    visit fale_mais_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Fale mai was successfully destroyed"
  end
end
