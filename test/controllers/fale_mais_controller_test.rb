require 'test_helper'

class FaleMaisControllerTest < ActionDispatch::IntegrationTest
  setup do
    @fale_mai = fale_mais(:one)
  end

  test "should get index" do
    get fale_mais_url
    assert_response :success
  end

  test "should get new" do
    get new_fale_mai_url
    assert_response :success
  end

  test "should create fale_mai" do
    assert_difference('FaleMai.count') do
      post fale_mais_url, params: { fale_mai: { minute: @fale_mai.minute, name: @fale_mai.name } }
    end

    assert_redirected_to fale_mai_url(FaleMai.last)
  end

  test "should show fale_mai" do
    get fale_mai_url(@fale_mai)
    assert_response :success
  end

  test "should get edit" do
    get edit_fale_mai_url(@fale_mai)
    assert_response :success
  end

  test "should update fale_mai" do
    patch fale_mai_url(@fale_mai), params: { fale_mai: { minute: @fale_mai.minute, name: @fale_mai.name } }
    assert_redirected_to fale_mai_url(@fale_mai)
  end

  test "should destroy fale_mai" do
    assert_difference('FaleMai.count', -1) do
      delete fale_mai_url(@fale_mai)
    end

    assert_redirected_to fale_mais_url
  end
end
