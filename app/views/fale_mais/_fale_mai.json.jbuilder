json.extract! fale_mai, :id, :name, :minute, :created_at, :updated_at
json.url fale_mai_url(fale_mai, format: :json)
