json.extract! rate, :id, :origin, :destiny, :price_per_minute, :created_at, :updated_at
json.url rate_url(rate, format: :json)
