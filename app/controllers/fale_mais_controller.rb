class FaleMaisController < ApplicationController 
  before_action :set_fale_mai, only: [:show, :edit, :update, :destroy]

  # GET /fale_mais
  # GET /fale_mais.json
  def index
    @fale_mai = FaleMai.all
  end

  # GET /fale_mais/1
  # GET /fale_mais/1.json
  def show
  end

  # GET /fale_mais/new
  def new
    @fale_mai = FaleMai.new
  end

  # /fale_mais/result
  def result    
   @fale_mai =  FaleMai.all
   @error= false

    if request.post? # Se consulta por post, faz a busca, de acordo com a Origin/Destiny/Plano.
      begin 
        @rates = Rate.where(origin: params['tax_origem'], destiny: params['tax_destino']).take
        @faleMais = FaleMai.new

       @comFaleMais = @faleMais.calculaPlano(params['minutos'].to_i, @rates.price_per_minute.to_f, params['fm_name'].to_i) # Resgata valor com Fale Mais
       @semFaleMais = @faleMais.calculaSemPlano(params['minutos'].to_i, @rates.price_per_minute.to_f) # Resgata valor sem Fale Mais
     rescue  
      @error = true;
    end

  end

end

  # GET /fale_mais/1/edit
  def edit
  end

  # POST /fale_mais
  # POST /fale_mais.json
  def create
    @fale_mai = FaleMai.new(fale_mai_params)

    respond_to do |format|
      if @fale_mai.save
        format.html { redirect_to @fale_mai, notice: 'Fale mai was successfully created.' }
        format.json { render :show, status: :created, location: @fale_mai }
      else
        format.html { render :new }
        format.json { render json: @fale_mai.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fale_mais/1
  # PATCH/PUT /fale_mais/1.json
  def update
    respond_to do |format|
      if @fale_mai.update(fale_mai_params)
        format.html { redirect_to @fale_mai, notice: 'Fale mai was successfully updated.' }
        format.json { render :show, status: :ok, location: @fale_mai }
      else
        format.html { render :edit }
        format.json { render json: @fale_mai.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fale_mais/1
  # DELETE /fale_mais/1.json
  def destroy
    @fale_mai.destroy
    respond_to do |format|
      format.html { redirect_to fale_mais_url, notice: 'Fale mai was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fale_mai
      @fale_mai = FaleMai.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fale_mai_params
      params.require(:fale_mai).permit(:name, :minute)
    end
  end
