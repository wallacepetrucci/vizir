class FaleMai < ApplicationRecord
	has_many :rate

	def calculaPlano(minutos, valorMinuto, minutoFaleMais)
		if(minutos <= minutoFaleMais)
			return 0
		end

		minutosExcedentes = minutos - minutoFaleMais # Descobre os Excendentes

		valorTaxaSoma = valorMinuto * 0.1

		valorMinuto = valorMinuto + valorTaxaSoma

		valorFaleMais = minutosExcedentes * valorMinuto

		return valorFaleMais

	end

	def calculaSemPlano(minutos, valorMinuto)

		valorSemFaleMais = minutos * valorMinuto

		return valorSemFaleMais
	end


end	
